module TerminalToFloat where 

    import TerminalSymbols
    import qualified Data.Map as Map
    import Data.Maybe

    type Dico = Map.Map String Float

    termListSplit :: Terminal -> [Terminal] -> [Terminal]
    termListSplit _ [] = []
    termListSplit term (t:ts)  
        | t==term = ts
        | otherwise = termListSplit term ts


    nSCRIPT :: [Terminal] -> ([String], [Terminal])
    nSCRIPT (t:ts)
        | elem t [Tid "", Tloop, Tdisp, Tral, Tend] && (head dTerm)==Tend = (dStk, dTerm)
        | otherwise = dispError t 
        where 
            (_, dStk, dTerm) = nLISTINSTR Map.empty [] (t:ts)


    nLISTINSTR :: Dico -> [String] -> [Terminal] -> (Dico, [String], [Terminal])
    nLISTINSTR dict dStk (t:ts)
        | elem t [Trbrak, Tend] = (dict, dStk, t:ts)
        | elem t [Tid "", Tloop, Tdisp, Tral] = nLISTINSTR newDict newStk newTs
        | otherwise = dispError t
        where 
            (newDict, newStk, newTs) = nINSTR dict dStk (t:ts)


    nINSTR :: Dico -> [String] -> [Terminal] -> (Dico, [String], [Terminal])
    nINSTR dict dstk (t:ts) 
        | isValidDisp= (dict, (show fe):dstk, tail ter)
        | isValidRal = (dict, "":dstk, tail ts)
        | isValidId  = (Map.insert id val dict, dstk, tail tpd) 
        | t==Tloop   = (dict, dstk, (concat (replicate nb tUntilSc)) ++ tAfterSc)
        | otherwise = dispError t
        where 
            isValidDisp = t==Tdisp && (head ter)==Tscolon
            isValidRal = t==Tral && (head ts)==Tscolon
            (fe, ter) = nE dict ts
            isValidId = (isId t) &&  (head ts)==Teq && (head tpd)==Tscolon
            (val, tpd) = nPDAFF dict (tail ts)
            id = getIdFromTerm t
            nb = round $ getFloatFromTerm (head ts)
            tUntilSc = take ( (length ts) - (length tAfterSc) -3) (tail $ tail ts)
            tAfterSc = termListSplit Trbrak (tail ts)

    nPDAFF :: Dico -> [Terminal] -> (Float, [Terminal])
    nPDAFF dict (t:ts) 
        | elem t [Tnb 0, Tid "", Trpar] = nE dict (t:ts) 
        | t==Tinv = (1/e, te)
        | t==Troot = (sqrt e, te)
        | otherwise = dispError t
        where 
            (e, te) = nE dict ts

    nE :: Dico -> [Terminal] -> (Float, [Terminal])
    nE dict (t:ts)
        | elem t [Tnb 0, Tid "", Tlpar] = (f+d, td)
        | otherwise = dispError t
        where 
            (f, tf) = nT dict (t:ts)
            (d, td) = nD dict tf

    nT :: Dico -> [Terminal] -> (Float, [Terminal])
    nT dict (t:ts) 
        | elem t [Tnb 0, Tid "", Tlpar] = (f*g, tg)
        | otherwise = dispError t
        where 
            (f, tf) = nF dict (t:ts)
            (g, tg) = nG dict tf

    nD :: Dico -> [Terminal] -> (Float, [Terminal])
    nD dict (t:ts) 
        | elem t [Trpar, Tscolon] = (0, t:ts) -- epsilon 
        | t==Tplus = nE dict ts
        | otherwise = dispError t

    nG :: Dico -> [Terminal] -> (Float, [Terminal])
    nG dict (t:ts)
        | elem t [Trpar, Tplus, Tscolon] = (1, t:ts) -- epsilon
        | t==Ttimes = nT dict ts
        | otherwise = dispError t

    nF :: Dico -> [Terminal] -> (Float, [Terminal])
    nF dict (t:ts) 
        | isNb t = (getFloatFromTerm t, ts)
        | isId t = (idVal, ts)
        | t==Tlpar && (head te)==Trpar = (e, tail te)
        | otherwise = dispError t
        where 
            (e, te) = nE dict ts
            idVal = fromMaybe (error "id does not exist") (Map.lookup (getIdFromTerm t) dict)