import qualified System.Environment as Sys

import TerminalSymbols
import TextToTerminal
import TerminalToFloat 

checkArgs :: [String] -> String
checkArgs [] = "pi212.txt" -- If nothing has been given run the pi-computing script
checkArgs [a] = a 
checkArgs as = error "Too many arguments provided."

-- goes through the display stack and prints results to be printed
displayResults :: [String] -> IO() 
displayResults [] = putStr ""
displayResults tStk = do putStrLn $ head tStk
                         displayResults $ tail tStk 

main::IO() 
main = do a <- Sys.getArgs
          let filename = checkArgs a
          program <- readFile filename
          let terminals = textToTerm program
          let (dispStk, _) = nSCRIPT terminals
          displayResults $ reverse dispStk
