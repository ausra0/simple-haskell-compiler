pi = 3.141592; 
sPi = 0; 
i = 1; 
parity = 1; 

boucle 50 
{
    invI2 = inv (i*i); 
    sPi = sPi + parity*invI2; 
    parity = parity * -1; 

    tmpPi = sPi * 12; 
    tmpPi = racine tmpPi; 

    afficher i; 
    aff_ral; 
    afficher pi + tmpPi * -1; 
    aff_ral; 
    afficher tmpPi; 
    aff_ral; 
    aff_ral; 

    i = i+1; 
}