module TextToTerminal where 

    {- This is the lexical analyser of our language -}

    import Data.Char
    import TerminalSymbols 

    -- NOTE : list recursions can be replaced by folds ;) 


    readId :: String -> (String, String) -- String -> (ReadId, RestOfString)
    readId [] = ([], [])
    readId xs
        | isAlphaNum x || elem x ['_', '-'] = (x:xn, xr)
        | otherwise = ([], xs)
        where 
            x = head xs
            (xn, xr) = readId (tail xs)

    readNumber :: String -> (String, String)
    readNumber [] = ([], [])
    readNumber xs
        | isNumber x || elem x ['.', '-'] = (x:xn, xr)
        | otherwise = ([], xs)
        where 
            x = head xs
            (xn, xr) = readNumber (tail xs)

    mapIdToToken :: String -> Terminal 
    mapIdToToken [] = error "invalid id"
    mapIdToToken xs 
        | xs == "afficher" = Tdisp
        | xs == "aff_ral" = Tral
        | xs == "boucle" = Tloop
        | xs == "inv" = Tinv 
        | xs == "racine" = Troot
        | otherwise = Tid xs

    getTermsFromText :: String -> [Terminal] -> [Terminal]
    getTermsFromText [] terms = Tend:terms
    getTermsFromText xs terms
        | x==' ' = getTermsFromText xr terms -- ignore spaces
        | x=='\n'= getTermsFromText xr terms -- ignore returns
        | x=='(' = getTermsFromText xr (Tlpar:terms)
        | x==')' = getTermsFromText xr (Trpar:terms)
        | x=='{' = getTermsFromText xr (Tlbrak:terms)
        | x=='}' = getTermsFromText xr (Trbrak:terms)
        | x=='=' = getTermsFromText xr (Teq:terms)
        | x=='*' = getTermsFromText xr (Ttimes:terms) 
        | x=='+' = getTermsFromText xr (Tplus:terms)
        | x==';' = getTermsFromText xr (Tscolon:terms) 
        | isAlpha x = getTermsFromText xidr (myid:terms)
        | isNumber x || x=='-' = getTermsFromText xnumr ((Tnb mynumber):terms)
        | otherwise = error ("Unknown symbol in : "++xs)
        where 
            x = head xs
            xr = tail xs
            -- handle for textual tokens 
            (xid, xidr) = readId xs 
            myid = mapIdToToken xid
            -- handle for numbers
            (xnum, xnumr) = readNumber xs -- read smthing that looks like a nb
            mynumber = read xnum :: Float -- check that xnum IS a float

    textToTerm :: String -> [Terminal]
    textToTerm xs = reverse (getTermsFromText xs [])

    termToText :: [Terminal] -> [String]
    termToText [] = []
    termToText terms = show(t):(termToText ts)
        where 
            t = head terms 
            ts = tail terms