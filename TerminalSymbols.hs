module TerminalSymbols where 

    import Data.List.Split 

    data Terminal = Tnb Float | Tid String | Tlpar | Trpar | Tlbrak | Trbrak | Teq | Ttimes | Tplus | Tscolon | Tinv | Troot | Tloop | Tdisp | Tral | Tend deriving (Read, Show)
    instance Eq Terminal where
        Tlpar == Tlpar = True
        Trpar == Trpar = True 
        Tlbrak == Tlbrak = True 
        Trbrak == Trbrak = True
        Teq == Teq = True 
        Ttimes == Ttimes = True
        Tplus == Tplus = True 
        Tscolon == Tscolon = True 
        Tinv == Tinv = True 
        Troot == Troot = True 
        Tloop == Tloop = True 
        Tdisp == Tdisp = True 
        Tral == Tral = True 
        Tend == Tend = True
        Tid a == Tid b = True
        Tnb a == Tnb b = True
        _ == _ = False 

    isNb :: Terminal -> Bool
    isNb t = t==(Tnb 0)

    isId :: Terminal -> Bool
    isId t = t==(Tid "")

    -- getTermValue :: Terminal -> String 
    -- getTermValue term = last $ splitOn " " (show term)

    getTermValue :: Tnb Float -> Float
    getTermValue term = last $ splitOn " " (show term)

    getFloatFromTerm :: Terminal -> Float
    getFloatFromTerm term = read (getTermValue term) :: Float
    
    getIdFromTerm :: Terminal -> String 
    getIdFromTerm term = removeQuotationMarks $ getTermValue term
        where removeQuotationMarks x = tail $ init x

    dispError :: Terminal -> a 
    dispError t = error ("Unexpected symbol : "++(show t))