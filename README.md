This is the implementation of a recursive descendent analyser for the grammar described in **tableau.pdf**. 
For additional details regarding the features and the construction of the compiler, please refer to this file too. 

The main file is pi.hs. 

To compile the program : 
ghc pi.hs 
(**prerequisits to compilation**)

To run the program : 
./pi "ProgramName.txt" 

If no filename is provided, the script **add** is executed by default. 

Other files are oganised as follows : 
TerminalSymbols.hs - description of terminal symbols **check le voc** 
TextToTerminal.hs  - converts the script provided by the user to a list of terminal symbols
TerminalToFloat.hs - executes the operations assiated to the terminal symbols
